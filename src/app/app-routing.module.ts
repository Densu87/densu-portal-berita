import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusinessnewsComponent } from './businessnews/businessnews.component';
import { EntertainmentnewsComponent } from './entertainmentnews/entertainmentnews.component';
import { GeneralnewsComponent } from './generalnews/generalnews.component';
import { HealthnewsComponent } from './healthnews/healthnews.component';
import { SciencenewsComponent } from './sciencenews/sciencenews.component';
import { SportnewsComponent } from './sportnews/sportnews.component';
import { TechnewsComponent } from './technews/technews.component';
import { TopheadingComponent } from './topheading/topheading.component';

const routes: Routes = [
  {path:'',component:TopheadingComponent},//home
  {path:'general',component:GeneralnewsComponent},//general
  {path:'tech',component:TechnewsComponent},//tech
  {path:'business',component:BusinessnewsComponent},//business
  {path:'entertainment',component:EntertainmentnewsComponent},//entertainment
  {path:'science',component:SciencenewsComponent},//science
  {path:'health',component:HealthnewsComponent},//health
  {path:'sports',component:SportnewsComponent},//sport

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
