import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsapiservicesService {

  constructor(private _http:HttpClient) { }

  //Api News
  newsApiUrl = "https://newsapi.org/v2/top-headlines?country=id&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
  //tech Api News
  techApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=technology&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //general Api News
  generalApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=general&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //entertaiment Api News
  entertainmentApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //bidiness Api News
  businessApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=business&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //sports Api News
  sportsApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=sports&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //science Api News
  scienceApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=science&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
  //health Api News
  healthApiUrl = "https://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=8ac1a988f57d44238eccb6f1d93ddc49";
 
 
 
 
  //Top Heading()
  topHeading():Observable<any>
  {
    return this._http.get(this.newsApiUrl);
  }
  //Tech News
  techNews():Observable<any>
  {
    return this._http.get(this.techApiUrl);
  }
  //general News
  generalNews():Observable<any>
  {
    return this._http.get(this.generalApiUrl);
  }
  //general News
  entertainmentNews():Observable<any>
  {
    return this._http.get(this.entertainmentApiUrl);
  }
  //bidiness News
  businessNews():Observable<any>
  {
    return this._http.get(this.businessApiUrl);
  }
  //sport News
  sportsNews():Observable<any>
  {
    return this._http.get(this.sportsApiUrl);
  }
  //science News
  scienceNews():Observable<any>
  {
    return this._http.get(this.scienceApiUrl);
  }
  //health News
  healthNews():Observable<any>
  {
    return this._http.get(this.healthApiUrl);
  }


}
